# Plano da Disciplina - Estrutura de Dados (FGA0147)

## Professores
* Fabricio Ataides Braz
* Nilton Correia da Silva

## Período
2º Semestre de 2.020

## Turmas
Turma 04

## Ementa
- Recursividade 
- Ponteiros e alocação dinâmica de memória 
- Estruturas lineares. Arrays. Listas. Filas. Pilhas 
- Introdução à Complexidade computacional e notação Big-O 
- Algoritmos de busca 
- Algoritmos de ordenação O(nˆ2) 
- Algoritmos em árvores binárias 
- Organização de arquivos 
- Aplicações 

## Método
* Aulas expositivas
* Atividades práticas de programação
* Trabalhos em grupo

## Avaliação

Para que o aluno seja aprovado na disciplina ele deve possuir desempenho superior ou igual a 50, correspondente a menção MM. Além disso, seu comparecimento deve ser superior ou igual a 75% dos eventos estabelecidos pela disciplina. 

### Desempenho

O desempenho dos alunos será verificado por meio de dois trabalhos de programação, sendo a média final igual a 0,4 x T1 + 0,6 x T2, onde T1 refere-se ao Trabalho 1 e T2 refere-se ao Trabalho 2.

Os trabalho serão realizados em grupo com 5 pessoas.

### Cronograma
|Aula | Data | Programação | Natureza |
| --- | --- | --- | --- |
| 1 | 2/2 | Cancelada | -  |
| 2 | 4/2 | Acolhida |  Síncrona  |
| 3 | 9/2 | Ponteiro |  Síncrona  |
| 4 | 11/2 |Alocação de variáveis e de vetores |  Síncrona  |
| | 16/2 | Feriado | - |
| 5 | 18/2 | Uso de ponteiros: passagem de parâmetros |  Síncrona  |
| 6 | 23/2 | Recursividade |  Síncrona  |
| 7 | 25/2 | Manipulação de Arquivos | Síncrona |
| 8 | 2/3 |*Atividade Prática* |  Assíncrona  |
| 9 | 4/3 | Complexidade e Notação Big-O |  Síncrona  |
| 10 | 9/3 | Análise de Complexidade de Códigos |  Assíncrona  |
| 11 | 11/3 | *Atividade Prática* | Assíncrona  |
| 12 | 16/3 | **Trabalho I (T-I)**  | Assíncrona |
| 13 | 18/3 | **Trabalho I (T-I)**  | Assíncrona |
| 14 | 23/3 | Algoritmos de Busca: linear e binária |  Síncrona  |
| 15 | 25/3 | Algoritmos de Ordenação: insert sort e bubble sort |  Síncrona  |
| 16 | 30/3 |  *Atividade Prática*  |  Assíncrona  |
| 17 | 1/4 | Listas Simples, Duplamente Encadeadas e Ciculares|  Síncrona  |
| 18 | 6/4 | Pilhas e Filas|  Síncrona  |
| 19 | 8/4 |  *Atividade Prática* |   Assíncrona  |
| 20 | 13/4 | Árvores Binárias |  Síncrona  |
| 21 | 15/4 | Árvores Binárias de Pesquisa: Inserções, Remoções |  Síncrona  |
| 22 | 20/4 | Árvores Binárias de Pesquisa: Busca, Travessia |  Síncrona  |
| 23 | 22/4 | Árvores AVL |  Síncrona  |
| 24 | 27/4 | Árvores AVL: Atividades |  Síncrona  |
| 25 | 29/4 | Árvores-B |  Síncrona  |
| 26 | 4/5 | Árvores-B: Algoritmos |  Síncrona  |
| 27 | 6/5 | Atividade Prática |   Assíncrona   |
| 28 | 11/5 | **Trabalho II (T-II)** |   Assíncrona   |
| 29 | 13/5 | **Trabalho II (T-II)** |   Assíncrona   |
| 30 | 18/5 | Fechamento: Revisão de notas e fechamento de média final |  Assíncrona  |

### Comparecimento

As atividades síncronas serão consideradas para contabilizar a presença.

### Evasão
Grupos com menos de 3 pessoas será extinto e o(s) seu(s) membro(s) serão distribuídos em grupos com menos integrantes.

## Referências Bibliográficas

* Básica: 

    * (eBrary) BALDWIN, D.; SCRAGG, G. Algorithms and Data Structures: The Science of Computing, 1st ed. Charles River Media, 2004. 
    * LAFORE, R. Estruturas de Dados e Algoritmos em Java, 1a. ed. Ciência Moderna, 2005. 
    * DROZDEK FERRAZ, Inhaúma Neves. Programação com arquivos. Barueri, SP: Manole, 2003. xvii, 345 p. ISBN 8520414893 

* Complementar: 
    * (eBrary) MEHLHORN, K; SANDERS, P. Algorithms and Data Structures: The Basic ToolBox, 1st. ed. Springer, 2008. 
    * (open access) AHO, A. V.; ULLMAN, J. D. Foundations of Computer Science: C Edition (Principles of Computer Science Series), 1st ed., W. H. Freeman, 1994. 
    * GUIMARÃES, A. M.; LAGES. N. A. C. Algoritmos e Estruturas de Dados, 1a. ed. LTC, 1994. 
    * SHERROD, A. Data Structures and Algorithms for Game Developers, 5th ed. Course Technology, 2007. 
    * (eBrary) DESHPANDE, P. S.; KAKDE, O. G. C and Data Structures, 1st ed. Charles River Media, 2004. 
    * (eBrary) DAS, V. V., Principles of Data Structures Using C and C++, 1s ed. New Age International, 2006. 
    * Alexander Kulikov and Pavel Pevzner. [Learning Algorithms through Programming and Puzzle Solving]()
